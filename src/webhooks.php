<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";

	if (!isset($_SESSION["account"]))
		header("Location: /") and die();


	$DB = new Database();

	function create($when, $endpoint) {
		global $DB;
		if (!$when or !$endpoint)
			return "You need to fill all the fields of the form!";

		if (!filter_var($endpoint, FILTER_VALIDATE_URL))
			return "The endpoint is not a valid URL!";

		$when = $DB->escape($when);
		$endpoint = $DB->escape($endpoint);
		$account_id = $_SESSION["account"]["id"];

		$DB->query(
			"INSERT INTO webhook (`when`, `endpoint`, `account_id`)
			VALUES ('$when', '$endpoint', $account_id)"
		);

		return "New webhook created!";
	}

	function _delete($id) {
		global $DB;
		if (!$id)
			return "An error occurred!";

		$id = $DB->escape($id);
		$account_id = $_SESSION["account"]["id"];

		$webhook = $DB->query(
			"SELECT id FROM webhook
			WHERE id = '$id' AND account_id = $account_id"
		);

		if (!$webhook)
			return "An error occurred!";

		$DB->query(
			"DELETE FROM webhook WHERE id = '$id'"
		);

		return "Webhook deleted!";
	}


	if (isset($_POST["add"]))
		die(create($_POST["when"], $_POST["endpoint"]));
	elseif (isset($_POST["delete"]))
		die(_delete($_POST["id"]));

	$account_id = $_SESSION["account"]["id"];
	$webhooks = $DB->query("SELECT * FROM webhook WHERE account_id = $account_id");

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Webhooks - On-Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="stylesheet" type="text/css" href="/css/table.css">

	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />

	<script type="text/javascript" src="/js/dialog.js" defer></script>
	<script type="text/javascript" src="/js/request.js"></script>
	<script type="text/javascript" src="/js/webhooks.js" defer></script>
</head>
<body>

	<main>
		<a href="/panel" id="back">Go back to the administration panel</a>
		<h1>Edit Webhooks</h1>
		<table>
			<thead>
				<tr>
					<th>Trigger</th>
					<th>Endpoint</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($webhooks as $webhook): ?>
					<tr>
						<td><?php switch ($webhook["when"]) {
							case "clock-in": echo "When a worker clocks in"; break;
							case "clock-out": echo "When a worker clocks out"; break;
							case "auto-clock-out": echo "When a worker clocks out automatically"; break;
							default: echo "???";
						} ?></td>
						<td><?php echo $webhook["endpoint"] ?></td>
						<td class="actions" data-webhook='<?php echo json_encode($webhook) ?>'>
							<i class="fa-solid fa-trash delete"></i>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<a id="add"><fa class="fa-solid fa-plus"></fa> Add a new webhook</a>
	</main>

</body>
</html>