<?php

namespace report;
require_once __ROOT__."/lib/PDF.php";


const CELL_WIDTH = 22.5;
const CELL_HEIGHT = 5;

function parseTime($seconds) {
	$hours = floor($seconds / 3600);
	$minutes = floor($seconds / 60) % 60;
	$seconds = $seconds % 60;

	$hours = substr('0'.$hours, -2);
	$minutes = substr('0'.$minutes, -2);
	$seconds = substr('0'.$seconds, -2);
	return "$hours:$minutes:$seconds";
}

function parseTimes($times) {
	$parsed = array();

	foreach ($times as $timecard)
		if ($timecard["type"] === "IN")
			array_push($parsed, array("IN"=>$timecard["timestamp"]));
		elseif ($timecard["type"] === "OUT")
			$parsed[count($parsed)-1]["OUT"] = $timecard["timestamp"];

	if (!$parsed[count($parsed)-1]["OUT"])
		array_pop($parsed);
	
	return $parsed;
}

function generateReport($user, $start, $end) {
	global $DB;
	//header('Content-Type: application/pdf');

	$pdf = new \PDF();
	$pdf->SetFont("Montserrat", "B", 12);
	$start_pretty = date("d/m/Y", strtotime($start));
	$end_pretty = date("d/m/Y", strtotime($end));
	$pdf->Cell(0, 10, "Report from $start_pretty to $end_pretty for $user[name]");
	$pdf->Ln(20);
	$table_top = $pdf->GetY();
	$pdf->SetFont("Montserrat", "B", 8);
	$pdf->SetFillColor(0x89, 0x64, 0x50);
	$pdf->SetTextColor(0xFF, 0xFF, 0xFF);

	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "DAY", 1, 0, 'L', true);
	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "CLOCK IN", 1, 0, 'L', true);
	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "CLOCK OUT", 1, 0, 'L', true);
	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "TOTAL TIME", 1, 0, 'L', true);
	$pdf->Ln(CELL_HEIGHT);

	$pdf->SetFont("courier", "", 8);
	$pdf->SetTextColor(0x00, 0x00, 0x00);


	$start = $DB->escape($start);
	$end = $DB->escape($end);

	$times = $DB->query(
		"SELECT `type`, `timestamp`
		 FROM timecard
		 WHERE user_token = '$user[token]'
		 AND `timestamp` BETWEEN '$start' AND '$end'"
	);

	$times = parseTimes($times);

	$total = 0;

	$column = 1;

	foreach ($times as $time) {
		if ($pdf->GetY() > 270 and $column === 1) {
			$column = 2;
			$pdf->SetXY(110, $table_top);
			$pdf->SetLeftMargin(110);
		} elseif ($pdf->GetY() > 270 and $column === 2) {
			$pdf->SetLeftMargin(10);
			$pdf->AddPage();
			$column = 1;
			$pdf->SetFont("Montserrat", "B", 13);
			$pdf->Cell(0, 10, "Report from $range for the user $user[name]");
			$pdf->Ln(20);
			$table_top = $pdf->GetY();
			$pdf->SetFont("Montserrat", "B", 8);
			$pdf->SetFillColor(0x89, 0x64, 0x50);
			$pdf->SetTextColor(0xFF, 0xFF, 0xFF);

			$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "DAY", 1, 0, 'L', true);
			$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "CLOCK IN", 1, 0, 'L', true);
			$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "CLOCK OUT", 1, 0, 'L', true);
			$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "TOTAL TIME", 1, 0, 'L', true);
			$pdf->Ln(CELL_HEIGHT);

			$pdf->SetFont("courier", "", 8);
			$pdf->SetTextColor(0x00, 0x00, 0x00);
		}

		$in = strtotime($time["IN"]);
		$out = strtotime($time["OUT"]);
		$worktime = $out - $in;
		$total += $worktime;

		$day = date("d/m/Y", strtotime($time["OUT"]));
		$clockin = date("H:i:s", $in);
		$clockout = date("H:i:s", $out);
		//$worktime = date("H:i:s", $worktime);
		$worktime = parseTime($worktime);

		$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, $day, 1);
		$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, $clockin, 1);
		$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, $clockout, 1);
		$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, $worktime, 1);
		$pdf->Ln(CELL_HEIGHT);
	}

	$pdf->SetFont("Montserrat", "B", 8);
	$pdf->SetFillColor(0x89, 0x64, 0x50);
	$pdf->SetTextColor(0xFF, 0xFF, 0xFF);

	//$pdf->setX(110 + CELL_WIDTH*2);
	$pdf->Cell(CELL_WIDTH*2, 0, null);
	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, "TOTAL", 1, 0, 'L', true);

	$pdf->SetFont("courier", "", 8);
	$pdf->SetTextColor(0x00, 0x00, 0x00);

	$pdf->Cell(CELL_WIDTH, CELL_HEIGHT, parseTime($total), 1);

	return $pdf;
}
