
function request(method="GET", endpoint="/api", params=null, action=null) {
	var request = new XMLHttpRequest()
	request.open(method, endpoint)
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			if (action) action(request.responseText)
		}
	}
	request.send(params)
}
