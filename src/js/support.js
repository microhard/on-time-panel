

var form = document.getElementsByTagName("form")[0]

form.onsubmit = function(e) {
	e.preventDefault()
	dialog("<center><img src='/img/loading.gif'></center>Please wait...", [])

	let message = document.getElementsByName("message")[0].value
	let email = document.getElementsByName("email")
	email = email.length > 0 ? `&email=${email[0].value}` : ""

	request(
		"POST",
		"/support",
		"message="+encodeURIComponent(message) + email,
		function(response) {
			dialog(false)
			dialog(response, [{"name": "OK", "action": function(){
				window.location = "/panel"
			}}])
		}
	)

	return false
}
